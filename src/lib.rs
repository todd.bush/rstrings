/// The `rstrings` module contains functions for finding strings within a reader.
use std::io::{BufReader, Error, ErrorKind, Read};
use std::str::FromStr;

pub enum NumberBase {
    Oct,
    Dec,
    Hex,
}

impl FromStr for NumberBase {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "oct" => Ok(NumberBase::Oct),
            "hex" => Ok(NumberBase::Hex),
            "dec" => Ok(NumberBase::Dec),
            _ => Err("Invalid variant name."),
        }
    }
}

#[derive(Clone, Debug)]
pub struct StringOffset {
    pub value: Vec<char>,
    pub offset: Option<usize>,
}

impl StringOffset {
    fn new() -> Self {
        StringOffset {
            value: Vec::new(),
            offset: None,
        }
    }
}

/// Returns a `Result` that contains either `Vec<StringOffset>` or `Error`.
///
/// # Examples
///
/// ```rust
/// use std::io::{BufReader, Error};
/// use std::io::Cursor;
///
/// use rstrings::find_strings_in_reader;
///
/// let data: Vec<u8> = vec![
///     0, 0, 0, 72, 101, 108, 108, 111, // Hello
///     0, 0, 119, 111, 114, 108, 100, // world
/// ];
/// let cursor = Cursor::new(data);
/// let reader = BufReader::new(cursor);
/// match find_strings_in_reader(reader, 4) {
///     Ok(matches) => { print!("number of strings: {}", matches.len()) }
///     Err(_) => {}
/// };
/// ```
pub fn find_strings_in_reader<R: Read>(
    reader: R,
    length: usize,
) -> Result<Vec<StringOffset>, Error> {
    if length < 1 {
        return Err(Error::new(
            ErrorKind::InvalidInput,
            "Value supplied for `length` is less than 1.",
        ));
    }

    let mut buf_reader = BufReader::new(reader);
    let mut buffer = Vec::new();

    match buf_reader.read_to_end(&mut buffer) {
        Ok(_) => {}
        Err(e) => return Err(e),
    }

    let mut data = StringOffset::new();
    let mut collection: Vec<StringOffset> = Vec::new();

    let mut p = buffer.iter().enumerate().peekable();
    while let Some((counter, &byte)) = p.next() {
        if (32..=126).contains(&byte) {
            if data.offset.is_none() {
                data.offset = Some(counter);
            }
            data.value.push(byte as char);
            if p.peek().is_none() && data.value.len() >= length {
                collection.push(data.clone());
            }
        } else if data.value.len() >= length {
            collection.push(data);
            data = StringOffset::new();
        } else if !data.value.is_empty() {
            data = StringOffset::new();
        }
    }

    Ok(collection)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Cursor;

    #[test]
    fn test_find_strings_in_file_true() {
        let data: Vec<u8> = vec![
            0, 0, 0, 72, 101, 108, 108, 111, // Hello
            0, 0, 119, 111, 114, 108, 100, // world
        ];
        let cursor = Cursor::new(data);
        let reader = BufReader::new(cursor);

        let result = find_strings_in_reader(reader, 4).unwrap();

        assert_eq!(result.len(), 2);
        assert_eq!(result[0].value.iter().collect::<String>(), "Hello");
        assert_eq!(result[1].value.iter().collect::<String>(), "world");
    }

    #[test]
    fn test_find_strings_in_file_false() {
        let data: Vec<u8> = vec![
            224, 205, 253, 20, 21, 212, 31, 156, 214, 196, 9, 21, 21, 144, 15, 177, 233, 203, 19,
            6, 217, 194, 14, 160, 15, 187, 24, 227, 246, 0, 2, 8, 165, 21, 6, 234, 18, 145, 0, 13,
            198, 169, 23, 145, 13, 248, 248, 10, 30, 198, 28, 30, 192, 15, 150, 209, 6, 20, 148,
            12, 25, 24, 223, 147, 21, 193, 24, 5, 25, 11, 28, 11, 199, 22, 149, 30, 22, 21, 220,
            156, 160, 218, 25, 13, 0, 153, 3, 2, 1, 221, 13, 156, 11, 10, 6, 8, 201, 206, 199, 23,
        ];
        let cursor = Cursor::new(data);
        let reader = BufReader::new(cursor);

        let result = find_strings_in_reader(reader, 1).unwrap();

        assert_eq!(result.len(), 0);
    }

    #[test]
    fn test_find_strings_in_file_empty() {
        let data: Vec<u8> = Vec::new();
        let cursor = Cursor::new(data);
        let reader = BufReader::new(cursor);

        let result = find_strings_in_reader(reader, 1).unwrap();

        assert_eq!(result.len(), 0);
    }

    #[test]
    fn test_find_strings_in_file_invalid_input() {
        let length = 0;

        let data: Vec<u8> = Vec::new();
        let cursor = Cursor::new(data);
        let reader = BufReader::new(cursor);

        let result = find_strings_in_reader(reader, length);

        assert!(result.is_err());
        let e = result.unwrap_err();
        assert_eq!(e.kind(), ErrorKind::InvalidInput);
    }
}
