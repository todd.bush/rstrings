use std::fs::File;
use std::str::FromStr;

use clap::{command, value_parser, Arg, ArgAction, Command};

use rstrings::{find_strings_in_reader, NumberBase};

fn main() -> Result<(), String> {
    let matches = cli().get_matches();
    let filenames = matches
        .get_many::<String>("filepath")
        .unwrap()
        .map(|s| s.as_str());

    let base = NumberBase::from_str(matches.get_one::<String>("offset").unwrap());
    let length = *matches.get_one::<usize>("length").unwrap();

    for filename in filenames {
        match File::open(filename) {
            Ok(file) => match find_strings_in_reader(file, length) {
                Ok(results) => {
                    for s in results {
                        let prefix = match base {
                            Ok(NumberBase::Oct) => format!("{:o} ", s.offset.unwrap()),
                            Ok(NumberBase::Dec) => format!("{} ", s.offset.unwrap()),
                            Ok(NumberBase::Hex) => format!("{:X} ", s.offset.unwrap()),
                            _ => "".to_string(),
                        };
                        let s: String = s.value.into_iter().collect();
                        println!("{}{}", prefix, s);
                    }
                }
                Err(e) => {
                    eprintln!("{}", e)
                }
            },
            Err(err) => {
                eprintln!("Error opening file {}: {}", filename, err)
            }
        }
    }

    Ok(())
}

fn cli() -> Command {
    let length = "4";

    command!()
        .about("rust strings ripoff")
        .arg_required_else_help(true)
        .arg(
            Arg::new("filepath")
                .num_args(1..)
                .action(ArgAction::Append)
                .help("Relative or full paths of the files to parse.")
                .value_parser(value_parser!(String)),
        )
        .arg(
            Arg::new("length")
                .long("length")
                .required(false)
                .default_value(length)
                .help("Minimum string length.")
                .value_parser(value_parser!(usize)),
        )
        .arg(
            Arg::new("offset")
                .long("offset")
                .required(false)
                .default_value("dec")
                .help("Display the string offset.")
                .value_parser(["dec", "oct", "hex"]),
        )
}
