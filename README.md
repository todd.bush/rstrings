# rstrings

A GNU `strings` ripoff implemented in Rust.

## Examples

### Example 1

Find strings with a minimum length of 5.

```shell
rstrings /usr/bin/strings --length 5
```

### Example 2

Find strings and print their offset in hexadecimal.

```shell
rstrings /usr/bin/strings --offset hex
```
